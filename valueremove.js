Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

var data = [0, 1, 2, 'stop', 2, 0, 1, 'stop']
console.log(data)

data.remove(0);
console.log(data)