from datetime import date, timedelta, datetime

curr = "1-1-1990"
end = "31-12-2000"
format = "%d-%m-%Y"
start_date = datetime.strptime(curr, format)
end_date = datetime.strptime(end, format)

step = timedelta(1)
num_thur = 0
off_days = ['Thu']

days = (end_date - start_date).days
for x in range(days):
    day = start_date.strftime("%a")
    if day in off_days:
        num_thur += 1
    start_date += step

print(num_thur)
